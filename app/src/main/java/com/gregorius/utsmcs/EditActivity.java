package com.gregorius.utsmcs;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class EditActivity extends AppCompatActivity {

    EditText etName, etNominal;
    String name;
    int nominal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        /* Call back button on action bar */
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);

        etName = findViewById(R.id.etName);
        etNominal = findViewById(R.id.etNominal);

        Intent intent = getIntent();
        int id = intent.getIntExtra("id", 0);

        SpendingHelper helper = new SpendingHelper(this);
        Spending spending = helper.select(id);

        String name = spending.getSpendingName();
        String nominal = String.valueOf(spending.getSpendingNominal());

        etName.setText(name);
        etNominal.setText(nominal);
    }

    public void update(View view) {
        Intent intent = getIntent();
        int id = intent.getIntExtra("id", 0);

        boolean anyErrors = false;

        name = etName.getText().toString();
        String nominalStr = etNominal.getText().toString();

        if(nominalStr.isEmpty()){
            etNominal.setError("Nominal must be filled!");
            etNominal.requestFocus();
            anyErrors = true;
        } else {
            nominal = Integer.parseInt(nominalStr);
            if(nominal == 0){
                etNominal.setError("Nominal can't be zero!");
                etNominal.requestFocus();
                anyErrors = true;
            }
        }

        if(name.isEmpty()){
            etName.setError("Expense name must be filled!");
            etName.requestFocus();
            anyErrors = true;
        }

        if(!anyErrors){
            SpendingHelper helper = new SpendingHelper(this);
            helper.update(id, name, nominal);
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}