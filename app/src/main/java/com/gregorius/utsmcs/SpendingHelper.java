package com.gregorius.utsmcs;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.Vector;

public class SpendingHelper {

    DBHelper dbHelper;

    public SpendingHelper(Context context){
        dbHelper = new DBHelper(context);
    }

    public void insert(String name, int nominal, String date){
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String nominalStr = String.valueOf(nominal);

        String[] insert = {name, nominalStr, date};

        db.execSQL("INSERT INTO spendings (name, nominal, date) VALUES (?,?,?)", insert);
        db.close();
        dbHelper.close();
    }

    public void update(int id, String name, int nominal){
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String idStr = String.valueOf(id);
        String nominalStr = String.valueOf(nominal);

        String[] update = {name, nominalStr, idStr};

        db.execSQL("UPDATE spendings SET name = ?, nominal = ? WHERE id = ?", update);
        db.close();
        dbHelper.close();
    }

    public Spending select(int id){
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String idStr = String.valueOf(id);
        String[] select = {idStr};

        Cursor cursor = db.rawQuery("SELECT * FROM spendings WHERE id = ?", select);

        cursor.moveToFirst();

        Spending spending = null;

        if(cursor.moveToFirst()){

            int spendingId = cursor.getInt(0);
            String name = cursor.getString(1);
            int nominal = cursor.getInt(2);
            String date = cursor.getString(3);

            spending = new Spending(spendingId, name, nominal, date);
        }

        cursor.close();
        db.close();
        dbHelper.close();

        return spending;
    }

    public Vector<Spending> selectAll(){
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Vector<Spending> spendings = new Vector<>();

        Cursor cursor = db.rawQuery("SELECT * FROM spendings", null);
        cursor.moveToFirst();

        if(cursor.moveToFirst()){
            while(!cursor.isAfterLast()){
                int spendingId = cursor.getInt(0);
                String name = cursor.getString(1);
                int nominal = cursor.getInt(2);
                String date = cursor.getString(3);

                spendings.add(new Spending(spendingId, name, nominal, date));
                cursor.moveToNext();
            }

        }
        cursor.close();
        db.close();
        dbHelper.close();

        return spendings;
    }

}
