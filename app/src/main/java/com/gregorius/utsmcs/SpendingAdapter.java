package com.gregorius.utsmcs;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Vector;

public class SpendingAdapter extends RecyclerView.Adapter<SpendingAdapter.ViewHolder> {

    Context context;
    Vector<Spending> spendings;

    public SpendingAdapter(Context context, Vector<Spending> spendings) {
        this.context = context;
        this.spendings = spendings;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvName, tvNominal, tvDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvNominal = itemView.findViewById(R.id.tvNominal);
            tvDate = itemView.findViewById(R.id.tvDate);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Spending spending = spendings.get(position);

            int id = spending.getSpendingId();
            Intent intent = new Intent(context, EditActivity.class);
            intent.putExtra("id", id);
            context.startActivity(intent);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.spending_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SpendingAdapter.ViewHolder holder, int position) {
        Spending spending = spendings.get(position);
        holder.tvName.setText(spending.getSpendingName());
        holder.tvNominal.setText("Rp " + spending.getSpendingNominal());
        holder.tvDate.setText(spending.getSpendingDate());
    }

    @Override
    public int getItemCount() {
        return spendings.size();
    }
}
