package com.gregorius.utsmcs;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.Vector;

public class MainActivity extends AppCompatActivity {

    /** rev 2
     * Gregorius Albert / 2301854486
     * LA03
     * UTS - Mobile Community Solution
     * Tested on Asus Zenfone 5Z Android 10 & AVD Pixel API 28-29
     */


    Vector<Spending> spendings = new Vector<>();
    RecyclerView rvSpending;
    SpendingAdapter spendingAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rvSpending = findViewById(R.id.rvSpending);
        render();
    }

    @Override
    protected void onResume() {
        super.onResume();
        render();
    }

    public void render(){
        SpendingHelper helper = new SpendingHelper(this);
        spendings = helper.selectAll();

        LinearLayoutManager manager = new LinearLayoutManager(this);
        rvSpending.setLayoutManager(manager);

        spendingAdapter = new SpendingAdapter(this, spendings);
        rvSpending.setAdapter(spendingAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.addMenu) {
            startActivity(new Intent(this, InsertActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
}