package com.gregorius.utsmcs;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class InsertActivity extends AppCompatActivity {

    EditText etName, etNominal;

    String name, dateString;
    int nominal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);

        /* Call back button on action bar */
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);

        etName = findViewById(R.id.etName);
        etNominal = findViewById(R.id.etNominal);
        etNominal.setText(String.valueOf(0));
    }

    public void submit(View view) {

        boolean anyErrors = false;

        name = etName.getText().toString();
        String nominalStr = etNominal.getText().toString();

        if(nominalStr.isEmpty()){
            etNominal.setError("Nominal must be filled!");
            etNominal.requestFocus();
            anyErrors = true;
        } else {
            nominal = Integer.parseInt(nominalStr);
            if(nominal == 0){
                etNominal.setError("Nominal can't be zero!");
                etNominal.requestFocus();
                anyErrors = true;
            }
        }

        if(name.isEmpty()){
            etName.setError("Expense name must be filled!");
            etName.requestFocus();
            anyErrors = true;
        }

        Date date = new Date();
        SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd");
        dateString = simpleDate.format(date);

        if(!anyErrors){
            SpendingHelper helper = new SpendingHelper(this);
            helper.insert(name, nominal, dateString);
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}