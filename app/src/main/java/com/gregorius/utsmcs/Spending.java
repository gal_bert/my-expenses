package com.gregorius.utsmcs;

public class Spending {

    private int spendingId;
    private String spendingName;
    private int spendingNominal;
    private String spendingDate;

    public Spending(int spendingId, String spendingName, int spendingNominal, String spendingDate) {
        this.spendingId = spendingId;
        this.spendingName = spendingName;
        this.spendingNominal = spendingNominal;
        this.spendingDate = spendingDate;
    }

    public int getSpendingId() {
        return spendingId;
    }

    public String getSpendingName() {
        return spendingName;
    }

    public int getSpendingNominal() {
        return spendingNominal;
    }

    public String getSpendingDate() {
        return spendingDate;
    }

}
